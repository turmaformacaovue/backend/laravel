<?php

namespace App\Http\Controllers;

use App\Models\StatusPagamento;
use Illuminate\Http\Request;

class StatusPagamentoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\StatusPagamento  $statusPagamento
     * @return \Illuminate\Http\Response
     */
    public function show(StatusPagamento $statusPagamento)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\StatusPagamento  $statusPagamento
     * @return \Illuminate\Http\Response
     */
    public function edit(StatusPagamento $statusPagamento)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\StatusPagamento  $statusPagamento
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StatusPagamento $statusPagamento)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\StatusPagamento  $statusPagamento
     * @return \Illuminate\Http\Response
     */
    public function destroy(StatusPagamento $statusPagamento)
    {
        //
    }
}
