<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Pedido extends Model
{
    use HasFactory;

    protected $fillable = [
        'statusPagamento_id',
        'user_id'        
    ];
    static public function setPedido($produtos)
    {

        $statusPagamento = 1;
            
            $pedido = Pedido::create([  
                'user_id' => auth()->user()->id,
                'statusPagamento_id' => $statusPagamento
                ]);
            foreach($produtos as $produto){
                PedidoProdutos::create([
                   'pedido_id' => $pedido->id,
                   'produto_id' => $produto['id']

                ]);

            } 

    }
    static public function getPedidos()
    {
        return DB::table('pedidos_produtos as pp')
                    ->join('pedidos as p', 'pp.venda_id', '=', 'p.id')
                    ->join('produtos as pr', 'pp.produto_id', '=', 'pr.id')
                    ->join('users as u', 'p.user_id', '=', 'u.id')
                    ->where('p.id', '=', auth()->user()->id)
                    ->select(
                        'u.name as usuario',
                        'pr.nome as produto',
                        'p.id as pedido'
                    )
                    ->get();
    }

}