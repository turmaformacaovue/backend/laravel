<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Produto extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'nome',
        'descricao',
        'preco'
    ];
    static public function setProduto($request)
    {
        return Produto::create($request->all());
    }
    static public function getProdutos()
    {
        return Produto::all();
    }

}