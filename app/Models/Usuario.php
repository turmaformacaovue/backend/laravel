<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Symfony\Component\HttpFoundation\Response;

use function PHPUnit\Framework\throwException;

class Usuario extends Model
{
    use HasFactory;

    protected $fillable = [
        'nome',
        'email',
        'password',
        'status'
    ];
    static public function setUsuario($request)
    {
        return Usuario::create($request->all());
    }

    static public function authenticateUSer($request)
    {
        //validate email, password from $request
        //acion method for validate
        //if -> false do return $resposta , Response::HTTP_UNAUTHORIZED
        //else -> true do return $resposta , Response::HTTP_ACCEPTED
    }

}
