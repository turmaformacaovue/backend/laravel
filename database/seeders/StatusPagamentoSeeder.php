<?php

namespace Database\Seeders;

use App\Models\StatusPagamento;
use Illuminate\Database\Seeder;

class StatusPagamentoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statusPagamento = [
            ['nome' => 'Aguardando Pagamento'],
            ['nome' => "Pagamento Confirmado"]
        ];
        foreach($statusPagamento as $s) {
            StatusPagamento::updateOrCreate($s);
        }
    }
}
