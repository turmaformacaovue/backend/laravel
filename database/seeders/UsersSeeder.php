<?php

namespace Database\Seeders;

use App\Models\User;
use GuzzleHttp\Promise\Create;
use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'name' => "Luiz",
                'email' => 'luiz@gmail.com',
                'password' => "12345L"
            ],
            [
                'name' => "Vitor",
                'email' => 'vitor@gmail.com',
                'password' => "12345V"
            ],
            [
                'name' => "Gustavo",
                'email' => 'gustavo@gmail.com',
                'password' => "12345G"
            ]
        ];
        foreach($users as $u) {
            User::updateOrCreate($u);
        }
        User::factory(10)->create();

    }
}
