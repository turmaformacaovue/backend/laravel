<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Produto;

class ProdutoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $produtos =  [
            [
                'nome' => "Mouse",
                'preco' => 150.00,
                'descricao' => "Mouse Gamer "
            ],
            [
                'nome' => "Tv",
                'preco' => 1250.00,
                'descricao' => "Tv bala"
            ],
            [
                'nome' => "Cadeira",
                'preco' => 250.00,
                'descricao' => "Cadeira Gamer"
            ],
            [
                'nome' =>  "Tv",
                'preco' => 1250.00,
                'descricao' => "Tv bala"
            ],
            [
                'nome' =>  "Notebook",
                'preco' => 3000.00,
                'descricao' => "Notebook RoG"
            ],
            [
                'nome' => "Rog Phone",
                'preco' => 5000.00,
                'descricao' => "Celular Phone"
            ],
                   [
                'nome' => "Tv",
                'preco' => 1250.00,
                'descricao' => "Tv bala"
            ],
            [
                'nome' => "Notebook",
                'preco' => 3000.00,
                'descricao' => "Notebook RoG"
            ]
        ];
            foreach($produtos as $p) {
                Produto::updateOrCreate($p);
            }
    }
}
