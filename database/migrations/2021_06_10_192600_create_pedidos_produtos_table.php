<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePedidosProdutosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedidos_produtos', function (Blueprint $table) {
            $table->id();
            
            $table->unsignedBigInteger('produto_id')->nullable()->constrained();
            $table->foreign('produto_id')->references('id')->on('produtos');

            $table->unsignedBigInteger('pedido_id')->nullable()->constrained();
            $table->foreign('pedido_id')->references('id')->on('pedidos');

            $table->integer('quant');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pedidos_produtos');
    }
}
